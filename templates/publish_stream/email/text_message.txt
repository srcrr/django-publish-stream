{{ entry.title }}

----

{{ entry.text_content }}

----

You are receiving this because you are subscribed to {{ newsletter.title }}.
To unsubscribe, please go to this page:

{% url "newsletter_unsubscribe_request" newsletter.slug %}

To manage options for this newsletter, please go to the following URL:

{% url "newsletter_update_request" newsletter.slug %}
