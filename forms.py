from django.forms import ModelForm
from publish_stream.models import SimpleNewsletterSubscription


class SubscriptionForm(ModelForm):
    class Meta:
        model = SimpleNewsletterSubscription
        fields = [
            "email",
        ]
