from django import test
import os
import random
from django.contrib.sites.models import Site
from django.contrib.auth.models import User
from publish_stream.models import TestPublicationChannel
from faker import Faker
from faker.providers import BaseProvider

import logging

log = logging.getLogger(__name__)

USE_ZINNIA = False
USE_MEZZANINE = False

try:
    from zinnia.models.entry import Entry

    USE_ZINNIA = True
except ImportError as ie:
    log.warning("NOT TESTING ZINNIA")

try:
    from mezzanine.blog.models import BlogPost

    USE_MEZZANINE = True
except ImportError as ie:
    log.warning("NOT TESTING MEZZANINE")

fake = Faker()


class ArticleProvider(BaseProvider):
    def article_title(self):
        return fake.sentence().rstrip(".").title()

    def article_content(self, np_min=5, np_max=7):
        return "\n\n".join(
            [fake.paragraph() for i in range(0, random.randint(np_min, np_max))]
        )


fake.add_provider(ArticleProvider)


class TestBlogTriggers(test.TestCase):
    def setUp(self):
        log.debug("Setting `DJANGO_MODE`")
        self.prev_django_mode = os.environ.get("DJANGO_MODE")
        os.environ["DJANGO_MODE"] = "test"

        self.user = User.objects.create(email=fake.email(), password=fake.password(),)

        super(TestBlogTriggers, self).setUp()

        self.site = Site.objects.create(domain=fake.domain_name(), name=fake.word())

        self.channel = TestPublicationChannel.objects.create(enabled=True)
        self.channel.sites.add(self.site)

    def tearDown(self):
        super(TestBlogTriggers, self).tearDown()
        if os.environ.get("GLOCK_TEST_ENTRY_ID"):
            del os.environ["GLOCK_TEST_ENTRY_ID"]
        if os.environ.get("DJANGO_MODE"):
            if self.prev_django_mode is None:
                log.debug("Unsetting `DJANGO_MODE`")
                del os.environ["DJANGO_MODE"]
            else:
                log.debug("Setting DJANGO_MODE back to %s", self.prev_django_mode)
                os.environ["DJANGO_MODE"] = self.prev_django_mode

    def test_simple_blog_post(self):
        log.debug("Saving blog post")
        post = BlogPost.objects.create(
            site_id=self.site.id,
            content=fake.article_content(),
            title=fake.article_title(),
            user=self.user,
        )
        self.assertIsNotNone(os.environ.get("GLOCK_TEST_ENTRY_ID", None))
