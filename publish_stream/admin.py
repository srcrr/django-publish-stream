from django.contrib import admin
from django import forms
from django.urls import reverse
from django.utils.safestring import mark_safe
from publish_stream.models import (
    PublishedArticleLog,
    ConsoleLogChannel,
    TumblrTextPublishChannel,
    ConsoleLogChannel,
    OauthRestChannelMixin,
    # EmailPublishChannel,
    USE_NEWSLETTER,
    USE_ZINNIA,
    # PublishableEntry,
)

if USE_NEWSLETTER:
    from publish_stream.models import EmailPublishChannel
if USE_ZINNIA:
    from publish_stream.admin.entry import EntryAdmin
    from publish_stream.models.entry import Entry


class OauthRestChannelMixinAdmin(admin.ModelAdmin):
    oauth_secret = forms.CharField(widget=forms.PasswordInput)
    consumer_secret = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = OauthRestChannelMixin


class PublishedArticleLogAdmin(admin.ModelAdmin):
    list_display = ("model_name", "model_id", "publish_date", "channel")

    class Meta:
        model = PublishedArticleLog


admin.site.register(TumblrTextPublishChannel, OauthRestChannelMixinAdmin)
admin.site.register(ConsoleLogChannel)
admin.site.register(PublishedArticleLog, PublishedArticleLogAdmin)
if USE_ZINNIA:
    admin.site.register(Entry, EntryAdmin)
if USE_NEWSLETTER:
    admin.site.register(EmailPublishChannel)
