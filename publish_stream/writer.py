import logging

log = logging.getLogger(__name__)

USE_MEZZANINE = False
try:
    from mezzanine.blog.models import BlogPost

    USE_MEZZANINE = True
except ImportError as err:
    log.warning("`mezzanine` not installed. `MezzanineStreamWriter` cannot be used")
    log.warning("To use `MezzanineStreamWriter` install the `Mezzanine` package:")
    log.warning("$ pip install mezzanine")

from publish_stream.models import PublicationChannel


class PublishStreamWriter:
    def write(self, entry_instance, publish_stream):
        raise NotImplementedError()


if USE_MEZZANINE:

    class MezzanineStreamWriter:
        def write(self, entry_instance, publish_stream):
            if not BlogPost in entry_instance.mro():
                raise AttributeError(
                    "%s is not an instance of %s", entry_instance, BlogPost
                )
            publication_channels = PublicationChannel.objects.all()
            for channel in publication_channels:
                if channel.gate and not channel.gate.does_pass(entry_instance):
                    log.info(
                        "Entry <%s> does not pass gate <%s>",
                        entry_instance,
                        channel.gate,
                    )
                    return
                channel.publish(entry_instance)
