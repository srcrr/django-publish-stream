import sys
import os
from django.db import models
from django.forms.widgets import PasswordInput
from django.conf import settings
from uuid import uuid4


from django.contrib.sites.models import Site

from polymorphic.models import PolymorphicModel
from django import template
import pytumblr

from django.utils.html import strip_tags
from django.core.mail import send_mail

import logging

log = logging.getLogger(__name__)
# print(f"This log name is '{__name__}'")

# Import some of the optional apps
USE_NEWSLETTER = False
try:
    from newsletter.models import Subscription, Newsletter

    USE_NEWSLETTER = True
except ImportError as err:
    log.warning("`newsletter` not imported. EmailPublishChannel will not be used.")
    log.warning("To use EmailPublishChannel, install `newsletter`:")
    log.warning("$ pip install django-newsletter")


USE_ZINNIA = False
try:
    from zinnia.models.entry import AbstractEntry

    USE_ZINNIA = True
except ImportError as err:
    log.warning("`zinnia` not imported. ZinniaPublishGate will not be used.")
    log.warning("To use ZinniaPublishGate, install `zinnia`:")
    log.warning("$ pip install django-blog-zinnia")


USE_MEZZANINE = False
try:
    from mezzanine.blog.models import BlogPost

    USE_MEZZANINE = True
except ImportError as err:
    log.warning("`mezzanine` not imported. MezzaninePublishGate will not be used.")
    log.warning("To use MezzaninePublishGate, install `zinnia`:")
    log.warning("$ pip install mezzanine")


# Determine if we're running unit tests

IS_TESTING = os.environ.get("DJANGO_MODE", None) == "test"

DEFAULT_SECRET = dict(max_length=2048)


class PublishedArticleLog(models.Model):
    """ A marker (or a log) to verify that we aren't republishing an article.

    By design the `Writer` will fill in these fields. The fields are
    intentionally ambiguous to allow for many types of django apps to publish
    (Zinnia and Mezzanine were the 2 I had in mind).

    If you're a Writer author, don't forget to update this model. :)

    :field model_name: Canonical name of the model for which an article was
    published.

    :field model_id: An identifier for the model. A text field instead of an
    integer field to allow for non-traditional fields, such as JSON for
    multi-columns.

    :field publish_date: When the publish occurred.

    :field channel: Channel that the model was published to.
    """

    model_name = models.CharField(max_length=1024)
    model_id = models.CharField(max_length=1024)
    publish_date = models.DateTimeField(auto_now=True)
    channel = models.ForeignKey("PublicationChannel", on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["model_name", "model_id"], name="unique_model_instance"
            ),
        ]


class PublicationChannelGate(PolymorphicModel):
    """ Whether to allow a publication channel to publish an entry.

    """

    def does_pass(self, entry_instance, **kwargs):
        raise NotImplementedError()


if USE_ZINNIA:

    class ZinniaChannelGate(PublicationChannelGate):

        pass  # TODO: refactor from old publication channel


if USE_MEZZANINE:

    class MezzanineChannelGate(PublicationChannelGate):

        pass  # TODO


class PublicationChannel(PolymorphicModel):
    """ Represents a place to repost an entry.

    :field enabled: Whether the publication channel is enabled or disabled.

    :field sites: For any entry that's published to a site, these will trigger
    the PublicationChannel to publish.
    """

    enabled = models.BooleanField(default=True)
    sites = models.ManyToManyField(Site, related_name="sites")
    gate = models.ForeignKey(
        PublicationChannelGate,
        related_name="+",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    def publish_zinnia(self, entry, **kwargs):
        """ Publish a zinnia article

        :param entry: BlogEntry entry

        :param kwargs: Any additional keyword arguments (for extensibility).
        """
        raise NotImplementedError()

    def publish_mezzanine(self, entry, **kwargs):
        """ Publish a Mezzanine article.

        :param entry: BlogPost entry

        :param kwargs: Any additional keyword arguments (for extensibility).
        """
        raise NotImplementedError()

    def publish(self, entry, **kwargs):
        """ Main `publish` method.

        This will be used for redirecting to the appropriate publisher (at this
        point Zinnia or Mezzanine).

        :param entry: The entry to publish (either BlogPost or BlogEntry depending
        on the platform)

        :param kwargs: Any additional keyword arguments (for extensibility).
        """
        if USE_ZINNIA:
            self.publish_zinnia(entry, **kwargs)
        if USE_MEZZANINE:
            self.publish_mezzanine(entry, **kwargs)


if IS_TESTING:

    class TestPublicationChannel(PublicationChannel):
        """ An even simpler publication channel that just returns entry id.

        Mainly used for unit tests. NOT INTENDED TO BE USED ELSEWHERE.
        """

        def publish_mezzanine(self, entry, **kwargs):
            os.environ["GLOCK_TEST_ENTRY_ID"] = str(entry.id)

        def publish_zinnia(self, entry, **kwargs):
            import ipdb

            ipdb.set_trace()
            pass


class ConsoleLogChannel(PublicationChannel):
    """A very simple console logging channel. Mainly used for tests."""

    def publish(self, entry):
        log.info("=" * 10)
        log.info("Entry published")
        log.info("-" * 10)
        log.info("Title: %s", entry.title)
        log.info("Content:")
        log.info("%s", entry.html_content)
        log.info("-" * 10)


class OauthRestChannelMixin(models.Model):
    """Any channel that uses Oauth authentication. """

    consumer_key = models.CharField(max_length=2048)
    consumer_secret = models.CharField(**DEFAULT_SECRET)
    oauth_token = models.CharField(max_length=2048)
    oauth_secret = models.CharField(**DEFAULT_SECRET)

    class Meta:
        abstract = True


class TumblrTextPublishChannel(PublicationChannel, OauthRestChannelMixin):
    """ Publishes to Tumblr.

    :field blog_name: The blog name to publish to. If not given, then this
    channel will attempt to discover it with client info and choose the
    first blog name.

    :field default_state: Where to post articles. Default is in the queue.

    """

    DEFAULT_PUBLISH_STATES = (
        ("published", "published"),
        ("draft", "draft"),
        ("queue", "queue"),
        ("private", "private"),
    )

    blog_name = models.CharField(max_length=1024, blank=True, null=True)

    default_state = models.CharField(
        max_length=20, choices=DEFAULT_PUBLISH_STATES, default="queue"
    )

    def get_client(self):
        return pytumblr.TumblrRestClient(
            self.consumer_key, self.consumer_secret, self.oauth_token, self.oauth_secret
        )

    def publish_mezzanine(self, entry, **kwargs):
        pass


if USE_NEWSLETTER:

    class EmailPublishChannel(PublicationChannel):
        from_email = models.EmailField()
        newsletter = models.ForeignKey(Newsletter, on_delete=models.CASCADE)

        def __str__(self):
            return f"EmailPublishChannel from <{self.from_email}>"
